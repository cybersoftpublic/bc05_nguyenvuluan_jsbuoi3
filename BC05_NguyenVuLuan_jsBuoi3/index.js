// bài tập 1
/*
iput: số ngày làm

step:tien lương * số ngày làm


output:tiền lương của nhân viên theo số ngày làm
*/
function tinhTienLuong() {
  var luongNgay = document.getElementById("luongNgay").value * 1;

  var soNgay = document.getElementById("day").value * 1;

  var tienLuong = 0;
  var tienLuong = luongNgay * soNgay;

  document.getElementById("result1").innerHTML = tienLuong;
}

// bài tập 2
/*
input: nhap gia tri cho 5 bien

step:
-tính tổng 5 biến
-tính trung bình (lấy tổng 5 biến chia 5)

output: ra gia tri trung binh
*/

function tinhTBSoThuc() {
  var soThuc1 = document.getElementById("soThuc1").value * 1;
  var soThuc2 = document.getElementById("soThuc2").value * 1;
  var soThuc3 = document.getElementById("soThuc3").value * 1;
  var soThuc4 = document.getElementById("soThuc4").value * 1;
  var soThuc5 = document.getElementById("soThuc5").value * 1;

  var giaTriTong = soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5;

  var giaTriTrungBinh = giaTriTong / 5;

  document.getElementById("result2").innerHTML = giaTriTrungBinh;
}

// bài tập 3
/*
ipput: nhập số tiền USD cần đổi

step:số tiền cần đổi * 235000

output: xuất ra số tiền VDN đã quy đổi
*/

var costUSD = 23500;

function doiTien() {
  var tienCanDoi = document.getElementById("tienCanDoi").value * 1;

  var tienDaDoi = tienCanDoi * costUSD;
  document.getElementById("result3").innerHTML = `${tienDaDoi} VND`;
}

// bai 4//

/*
ipput: nhập chiều dài + rộng

step:
-tính số diện tích (dài * rộng)
-tính chu vi ( dài + rộng /2)

output: xuất ra diện tích và chu vi
*/
function tinhChuViVaDienTich() {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;

  var chuVi = (chieuDai + chieuRong) * 2;
  var dienTich = chieuDai * chieuRong;
  document.getElementById(
    "result4"
  ).innerHTML = `Chu Vi: ${chuVi} và Diện Tích: ${dienTich}`;
}

// bai 5//

/*
ipput: nhập vào 1 số co 2 chữ số

step:
-tính tổng 2 ký số

output: tổng 2 ký số
*/

function tinhTongKySo() {
  var number = document.getElementById("number1").value * 1;
  var number1 = number % 10;

  var number2 = Math.floor(number / 10);

  var tongKySo = number1 + number2;
  document.getElementById("result5").innerHTML = tongKySo;
}
